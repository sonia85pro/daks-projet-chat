const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const path = require("path");
const axios = require('axios');



app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static("public"));

// définie le moteur de vue pour les template ejs
app.set("view engine", "ejs");
// Chemin des vues
app.set("views", path.join(__dirname, "views"));

app.get("/", (req, res) => {
  res.render("index");


});

app.get("/login", (req, res) => {
  res.render("login");
});

app.listen(3000, () => {
  console.log("Serveur demarré");
});
