CREATE TABLE IF NOT EXISTS `mydb`.`users` (
  `users_id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(30) NOT NULL,
  `mail` VARCHAR(255) NOT NULL,
  `password` VARCHAR(12) NOT NULL,
  `messages_id` INT NOT NULL,
  PRIMARY KEY (`users_id`, `messages_id`),
  UNIQUE INDEX `mail_UNIQUE` (`mail` ASC),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_users_messages_idx` (`messages_id` ASC),
  UNIQUE INDEX `messages_messages_id_UNIQUE` (`messages_id` ASC),
  CONSTRAINT `fk_users_messages`
    FOREIGN KEY (`messages_id`)
    REFERENCES `mydb`.`messages` (`messages_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB